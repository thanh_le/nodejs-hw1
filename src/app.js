const express = require('express');
const app = express();
const PORT = 8080;
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const morgan = require('morgan');

app.use(bodyParser.json());
app.use(morgan('dev'));

const folderDir = path.join(__dirname, 'fileStore');

app.post('/api/files', (req, res) => {
  const fileExtension = req.body.filename.split('.')[1];
  const acceptedExtension = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
  const fileName = req.body.filename;
  const fileContent = req.body.content;
  const fileDir = `${folderDir}/${fileName}`;
  if (!acceptedExtension.includes(fileExtension) || !fileContent) {
    res.status(400).send('Data is not accepted!');
  } else {
    if (fs.existsSync(fileDir)) {
      res.status(400).send('File name was exist!');
    } else {
      // const fileName = req.body.filename;
      // const fileContent = req.body.content;
      // const fileDir = `${folderDir}/${fileName}`;
      fs.appendFile(fileDir, fileContent, function (err) {
        if (err) throw err;
        res.status(200).send('File was created!');
      });
    }
  }
});

app.get('/api/files', (req, res) => {
  const data = [];
  const filesName = fs.readdirSync(folderDir);

  filesName.forEach((fileName) => {
    const fileDir = `${folderDir}/${fileName}`;
    const fileContent = fs.readFileSync(fileDir);
    data.push({ filename: fileName, content: fileContent.toString() });
  });
  res.status(200).json(data);
});

app.get('/api/files/:filename', (req, res) => {
  const fileName = req.params.filename;
  const fileDir = `${folderDir}/${fileName}`;
  try {
    if (!fs.existsSync(fileDir)) {
      res.status(400).send('File not found!');
    } else {
      res.status(200).sendFile(fileDir);
    }
  } catch (err) {
    console.error(err);
  }
});

app.listen(PORT, () => {
  console.log(`App is listening at port ${PORT}`);
});
